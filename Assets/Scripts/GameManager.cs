﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public Bird[] birdsPrefabs;
    public float spawnTime;
    public int timeLimit;

    int m_curTimeLimit;
    int m_birdKilled;

    bool m_isGameover;

    public bool IsGameover { get => m_isGameover; set => m_isGameover = value; }
    public int BirdKilled { get => m_birdKilled; set => m_birdKilled = value; }

    public override void Awake()
    {
        MakeSingleton(false);
        m_curTimeLimit = timeLimit; 
    }
    public override void Start()
    {
        GameGUIManager.Ins.ShowGameGUI(false);
        GameGUIManager.Ins.UpDateKilledCouting(m_birdKilled);
        
    }
    public void PlayGame()
    {
        StartCoroutine(GameSpawn());
        StartCoroutine(TimeCountDown());
        GameGUIManager.Ins.ShowGameGUI(true);
    }

    IEnumerator TimeCountDown()
    {
        while(m_curTimeLimit > 0)
        {
            yield return new WaitForSeconds(1f);
            m_curTimeLimit--;

            if (m_curTimeLimit <=0)
            {
                m_isGameover = true;

                if (m_birdKilled > Prefs.bestScore)
                {
                     GameGUIManager.Ins.gameDiaLog.UpdateDiaLog("NEW BEST","BEST KILLES : X" + m_birdKilled);
                }
                else 
                {
                     GameGUIManager.Ins.gameDiaLog.UpdateDiaLog("YOUR BEST","BEST KILLES : X" + Prefs.bestScore);
                }

                Prefs.bestScore = m_birdKilled;

                GameGUIManager.Ins.gameDiaLog.ShowDialog(true);
                GameGUIManager.Ins.CurDialog = GameGUIManager.Ins.gameDiaLog;
            }
            GameGUIManager.Ins.UpdateTimer(IntToTime(m_curTimeLimit));
        }
    }
    IEnumerator GameSpawn()
    {
        while(!m_isGameover)
        {
            SpawnBird();
            yield return new WaitForSeconds(spawnTime);
        }
    }
    void SpawnBird()
    {
        Vector3 spawnPos = Vector3.zero;
        float randCheck = Random.Range(0f,1f);

        if (randCheck >= 0.5f)
        {
            spawnPos = new Vector3 (10, Random.Range(1.5f, 4f),0);
        }
        else
        {
            spawnPos =new Vector3 (-10, Random.Range(1.5f, 4f),0);
        }

        if (birdsPrefabs != null && birdsPrefabs.Length > 0)
        {
            int randIndx = Random.Range(0, birdsPrefabs.Length);

            if (birdsPrefabs[randIndx] != null)
            {
                Bird birdClone = Instantiate(birdsPrefabs[randIndx], spawnPos,Quaternion.identity);
            }
        }

    }
    string IntToTime(int time)
    {
        float minute = Mathf.Floor(time / 60);
        float second = Mathf.RoundToInt(time % 60);

        return minute.ToString("00") + " : " + second.ToString("00");
    }
}
